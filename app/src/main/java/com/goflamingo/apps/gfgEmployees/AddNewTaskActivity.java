package com.goflamingo.apps.gfgEmployees;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.Toast;

import com.goflamingo.apps.gfgEmployees.model.Tasks;
import com.goflamingo.apps.gfgEmployees.network.GFGNetworkAPIService;
import com.goflamingo.apps.gfgEmployees.util.CacheManager;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddNewTaskActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

    public List<String> mEmployees, mPoints;
    private CacheManager mCacheManager;
    private Spinner mEmployeeSpinner, mPointSpinner;
    private String mEmployee_pk, mpoint_pk;
    private EditText mTask;
    private ProgressBar mProgressbar;
    private  String mEmployee;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_new_task);
        SplashActivity.setupUI(findViewById(R.id.add_tender_layout), AddNewTaskActivity.this);
        mCacheManager = new CacheManager(this);
        mEmployeeSpinner = findViewById(R.id.employee_spinner);
        mPointSpinner = findViewById(R.id.points_spinner);
        mProgressbar = findViewById(R.id.simpleProgressBar);
        mProgressbar.setVisibility(View.GONE);
        mTask = findViewById(R.id.task);
        mCacheManager = new CacheManager(getApplicationContext());
        // Spinner Drop down elements Mills
        mEmployeeSpinner.setOnItemSelectedListener(this);
        mEmployees = new ArrayList<String>();
        mEmployees.add("None");
        for (int i = 0; i < mCacheManager.getEmployees().size(); i++) {
            mEmployees.add(mCacheManager.getEmployees().get(i).getName());
        }
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, mEmployees);
        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // attaching data adapter to spinner
        mEmployeeSpinner.setAdapter(dataAdapter);
        mEmployeeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
           @Override
           public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
               mEmployee_pk = "";
               for(int i=0;i<mCacheManager.getEmployees().size();i++){
                   if(mEmployeeSpinner.getSelectedItem().equals(mCacheManager.getEmployees().get(i).getName())){
                       mEmployee_pk = mCacheManager.getEmployees().get(i).getId();
                   }
               }
           }
           @Override
           public void onNothingSelected(AdapterView<?> parent) {

           }
       });
        mPoints = new ArrayList<String>();
        for(int i =0;i<mCacheManager.getPoints().size();i++) {
            mPoints.add(mCacheManager.getPoints().get(i).getName());
        }

        ArrayAdapter<String> paymentDataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, mPoints);
        // Drop down layout style - list view with radio button
        paymentDataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // attaching data adapter to spinner
        mPointSpinner.setAdapter(paymentDataAdapter);
        mPointSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                String mSelectedPoint = mPoints.get(i);
                for (int j = 0; j < mCacheManager.getPoints().size(); j++) {
                    if (mSelectedPoint.equals(mCacheManager.getPoints().get(j).getName())) {
                        mpoint_pk = mCacheManager.getPoints().get(j).getId();
                    }
                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }
    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    private void postTask(){
        mProgressbar.setVisibility(View.VISIBLE);
        GFGNetworkAPIService.DeviceApi apiService = GFGNetworkAPIService.getInstance().newTransaction(this);
        Call<Tasks> postData = apiService.postTask(mCacheManager.getEmployeepk(),mEmployee_pk,mTask.getText().toString(), mpoint_pk);
        postData.enqueue(new Callback<Tasks>() {
            @Override
            public void onResponse(Call<Tasks> call, Response<Tasks> response) {
                mProgressbar.setVisibility(View.GONE);
                if (!call.isCanceled() && response.isSuccessful()) {
                    Log.i("selectd","selecting"+response.body());
                    Toast.makeText(getApplicationContext(),getResources().getString(R.string.task_added),Toast.LENGTH_LONG).show();
                    finish();
                }else{
                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        Toast.makeText(getApplicationContext(), jObjError.getString("detail"), Toast.LENGTH_LONG).show();
                    } catch (Exception e) {
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.please_try_again), Toast.LENGTH_LONG).show();
                        Log.i("Error","Retrofit error"+e.getMessage());
                    }
                }
            }
            @Override
            public void onFailure(Call<Tasks> call, Throwable t) {
                mProgressbar.setVisibility(View.GONE);
                Log.i("Error","error"+t.getMessage());
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.please_try_again), Toast.LENGTH_LONG).show();
            }
        });
    }
    public void addTask(View v){
        if(validate(mTask))  {
            postTask();
        }
    }
    private boolean validate(EditText editText) {
        if (editText.getText().toString().trim().length() < 1) {
            editText.setError(getResources().getString(R.string.please_fill_text));
            editText.requestFocus();
            return false;
        }
        return true;
    }

}
