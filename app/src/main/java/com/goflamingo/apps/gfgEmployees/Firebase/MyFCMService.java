package com.goflamingo.apps.gfgEmployees.Firebase;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.util.Log;

import com.goflamingo.apps.gfgEmployees.MainActivity;
import com.goflamingo.apps.gfgEmployees.util.CacheManager;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

public class MyFCMService extends FirebaseMessagingService {

    CustomNotifications mCustomNotification;

    CacheManager mCachemanager;

    @Override
    public void onNewToken(String s) {
        mCachemanager = new CacheManager(this);
        mCachemanager.setIsTokenGenerated(true);
        mCachemanager.setFCMToken(FirebaseInstanceId.getInstance().getToken());
        Log.i("On Token Refresh", "tokennnnnnnnnnnnn" + FirebaseInstanceId.getInstance().getToken());
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

        mCustomNotification = new CustomNotifications(this);
        Log.i("Notification type--", "-----------------   " + remoteMessage.getData().get("notification_type"));
        if (remoteMessage.getData().get("notification_type").equals("new_task")) {
            Intent i = new Intent(this, MainActivity.class);
            i.putExtra("id", remoteMessage.getData().get("to_do_id"));
            mCustomNotification.showNotification("New Task", remoteMessage.getData().get("assigned_by") + "-" + remoteMessage.getData().get("task_desc"), convertDate(remoteMessage.getData().get("date")),remoteMessage.getData().get("priority"), i);
        } else if (remoteMessage.getData().get("notification_type").equals("task_update")) {
            Intent i = new Intent(this, MainActivity.class);
            if(remoteMessage.getData().get("status").equals("Doing")){
                mCustomNotification.showNotification("started working on this task", remoteMessage.getData().get("assigned_to"), remoteMessage.getData().get("task_desc"), remoteMessage.getData().get("status"), i);
            }else if(remoteMessage.getData().get("status").equals("Done")){
                mCustomNotification.showNotification("Task has been completed", remoteMessage.getData().get("assigned_to"), remoteMessage.getData().get("task_desc"), remoteMessage.getData().get("status"), i);
            }else if(remoteMessage.getData().get("status").equals("Verified")){
                mCustomNotification.showNotification("Your task has been verified", remoteMessage.getData().get("assigned_to"), remoteMessage.getData().get("task_desc"), remoteMessage.getData().get("status"), i);
            }

        }
    }

    private String convertDate(String db_date) {
        SimpleDateFormat outputFormat = new SimpleDateFormat("dd-MM-yyyy  hh:mm a", Locale.ENGLISH);
        Date date2 = new Date();
        try {

            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
            sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
            String data2 = db_date + ".000Z";
            date2 = sdf.parse(data2);
            outputFormat.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"));


        } catch (Exception e) {
            Log.i("errorrrr", "error" + e.getMessage());
        }
        return outputFormat.format(date2);
    }
}
