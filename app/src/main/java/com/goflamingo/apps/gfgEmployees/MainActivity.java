package com.goflamingo.apps.gfgEmployees;


import android.app.Fragment;
import android.app.FragmentManager;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;


import com.goflamingo.apps.gfgEmployees.adapter.RecyclerAdapterDoneTasks;
import com.goflamingo.apps.gfgEmployees.fragments.AllTasksFragment;
import com.goflamingo.apps.gfgEmployees.fragments.DoneFragment;
import com.goflamingo.apps.gfgEmployees.fragments.MoreFragment;
import com.goflamingo.apps.gfgEmployees.fragments.ToDoFragment;
import com.goflamingo.apps.gfgEmployees.fragments.DoingFragment;
import com.goflamingo.apps.gfgEmployees.model.AllTasks;
import com.goflamingo.apps.gfgEmployees.model.Tasks;
import com.goflamingo.apps.gfgEmployees.network.GFGNetworkAPIService;
import com.goflamingo.apps.gfgEmployees.util.CacheManager;

import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity implements MoreFragment.OnFragmentInteractionListener {
    private static final String LOGTAG = MainActivity.class.getSimpleName();
    public static   AlertDialog.Builder alertDialog,builder;
    private CacheManager mCachManger;
    public static Menu menu;
    Toolbar myToolbar;
    TextView title;
    String status = "Done";


    final AllTasksFragment allTasksFragment = new AllTasksFragment();
    final ToDoFragment toDoFragment = new ToDoFragment();
    final DoingFragment doingFragment = new DoingFragment();
    final MoreFragment moreFragment = new MoreFragment();
    final DoneFragment doneFragment = new DoneFragment();
    FragmentManager fm =getFragmentManager();
    Fragment defaultFragment = allTasksFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mCachManger = new CacheManager(this);



        allDoneTasks();

        fm.beginTransaction().add(R.id.container,moreFragment,"3").hide(moreFragment).commit();
        fm.beginTransaction().add(R.id.container, doingFragment,"3").hide(doingFragment).commit();
        fm.beginTransaction().add(R.id.container, toDoFragment,"2").hide(toDoFragment).commit();
        fm.beginTransaction().add(R.id.container, doneFragment,"2").hide(doneFragment).commit();
        fm.beginTransaction().add(R.id.container, allTasksFragment,"1").commit();


        myToolbar = findViewById(R.id.my_toolbar);
        title = myToolbar.findViewById(R.id.pageTitle);
        setSupportActionBar(myToolbar);

        setupNavigationView();


        if(!mCachManger.isTokenAddedToDB()){

            postTokenToDB();
        }

        alertDialog=new AlertDialog.Builder(MainActivity.this);
        builder = new AlertDialog.Builder(MainActivity.this);
    }
    public void setTitle(String title) {
        this.title.setText(title);
    }

    private void setupNavigationView() {
        BottomNavigationView bottomNavigationView = findViewById(R.id.navigation);
        if (bottomNavigationView != null) {
            // Select first menu item by default and show Fragment accordingly.
            menu = bottomNavigationView.getMenu();
//            MenuItem mtodo = menu.findItem(R.item.)
            selectFragment(menu.getItem(0));
            // Set action to perform when any menu-item is selected.
            bottomNavigationView.setOnNavigationItemSelectedListener(
                    new BottomNavigationView.OnNavigationItemSelectedListener() {
                        @Override
                        public boolean onNavigationItemSelected(@NonNull MenuItem item) {

                            selectFragment(item);
                            return false;
                        }
                    });
        }
    }

    /**
     * Perform action when any item is selected.
     *
     * @param item Item that is selected.
     */
    protected void selectFragment(MenuItem item) {
        item.setChecked(true);
        switch (item.getItemId()) {
            case R.id.todo:
                fm.beginTransaction().hide(defaultFragment).show(toDoFragment).commit();
                defaultFragment = toDoFragment;
                setTitle(getResources().getString(R.string.to_do));
                break;
            case R.id.doing:
                fm.beginTransaction().hide(defaultFragment).show(doingFragment).commit();
                defaultFragment = doingFragment;
                setTitle(getResources().getString(R.string.doing));
                break;
            case R.id.navigation_done:
                fm.beginTransaction().hide(defaultFragment).show(doneFragment).commit();
                defaultFragment = doneFragment;
                setTitle(getResources().getString(R.string.done_tasks));
                break;
            case R.id.tasks:
                fm.beginTransaction().hide(defaultFragment).show(allTasksFragment).commit();
                defaultFragment = allTasksFragment;
                setTitle(getResources().getString(R.string.all_tasks_toolbar));
                break;
//            case R.id.navigation_more:
//                fm.beginTransaction().hide(defaultFragment).show(moreFragment).commit();
//                defaultFragment = moreFragment;
////                setTitle(getResources().getString(R.string.more_toolbar));
//                break;
        }
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    public void postTokenToDB(){
        GFGNetworkAPIService.DeviceApi apiService = GFGNetworkAPIService
                .getInstance().newTransaction(this);
        Call<String> call = apiService.postFCMToken(mCachManger.getFCMToken(),mCachManger.getEmployeepk(), Build.SERIAL);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                if (!call.isCanceled() && response.isSuccessful()) {
                    mCachManger.setIsTokenAddedToDB(true);
                }else{
                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        Toast.makeText(getApplicationContext(), jObjError.getString("detail"), Toast.LENGTH_LONG).show();
                    } catch (Exception e) {
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.please_try_again), Toast.LENGTH_LONG).show();
                        Log.i("Error","Retrofit error"+e.getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Toast.makeText(getApplicationContext(),  getResources().getString(R.string.please_try_again), Toast.LENGTH_LONG).show();
                Log.i("error","error"+t.getMessage());
            }
        });

    }

    public void addNewTask(View view) {
        Intent i = new Intent(this, AddNewTaskActivity.class);
        startActivity(i);
    }
    private void allDoneTasks() {

        GFGNetworkAPIService.DeviceApi apiService = GFGNetworkAPIService
                .getInstance().newTransaction(getApplicationContext());
        Call<Tasks> call = apiService.getAlertDoneTasks(status,mCachManger.getEmployeepk());
        call.enqueue(new Callback<Tasks>() {
            @Override
            public void onResponse(Call<Tasks> call, final Response<Tasks> response) {
                if(!call.isCanceled() && response.isSuccessful()){
                    for(int i = 0 ;i<response.body().getResults().size();i++){
                        if(status.equals(response.body().getResults().get(i).getStatus())){
                            AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                            builder.setTitle(response.body().getResults().get(i).getTo_emp().getName());
                            builder.setMessage(response.body().getResults().get(i).getDesc());
                            final String task_pk =  response.body().getResults().get(i).getId();
                            builder.setPositiveButton("Verify", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    verifiedTask("Verified",task_pk);
                                    dialog.dismiss();
                                }
                            });
                            builder.setNegativeButton("reOpen", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    verifiedTask("ToDo",task_pk);
                                    dialog.dismiss();

                                }
                            });
                            builder.setNeutralButton("skip", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    Toast.makeText(getApplicationContext(),"skipped",Toast.LENGTH_LONG).show();
                                    dialog.dismiss();
                                }
                            });
                            AlertDialog builelder = builder.create();
                            builelder.show();
                        }
                        else{
                            Toast.makeText(getApplicationContext(),"no finished Tasks",Toast.LENGTH_LONG).show();
                        }
                    }
                }else {
                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        Toast.makeText(getApplicationContext(), jObjError.getString("detail"), Toast.LENGTH_LONG).show();
                    } catch (Exception e) {
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.please_try_again), Toast.LENGTH_LONG).show();
                        Log.i("Error","Retrofit error"+e.getMessage());
                    }
                }

            }

            @Override
            public void onFailure(Call<Tasks> call, Throwable t) {
                Log.i("error", "error" + t.getMessage());
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.please_try_again), Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void verifiedTask(final String status,String task_pk){
        GFGNetworkAPIService.DeviceApi apiService = GFGNetworkAPIService.getInstance().newTransaction(getApplicationContext());
        Call<AllTasks> call = apiService.postEndStatus(task_pk,status, mCachManger.getEmployeepk(),"");
        call.enqueue(new Callback<AllTasks>() {
            @Override
            public void onResponse(Call<AllTasks> call, Response<AllTasks> response) {
                if(!call.isCanceled()&&response.isSuccessful()){
                    Toast.makeText(getApplicationContext(),status,Toast.LENGTH_LONG).show();
                }else{
                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        Toast.makeText(getApplicationContext(), jObjError.getString("detail"), Toast.LENGTH_LONG).show();
                    } catch (Exception e) {
                        Toast.makeText( getApplicationContext(),getResources().getString(R.string.please_try_again), Toast.LENGTH_LONG).show();
                        Log.i("Error", "Retrofit error" + e.getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<AllTasks> call, Throwable t) {
                Log.i("error", "error" + t.getMessage());
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.please_try_again), Toast.LENGTH_SHORT).show();
            }
        });
    }


}
