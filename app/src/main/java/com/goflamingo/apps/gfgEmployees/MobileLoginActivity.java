package com.goflamingo.apps.gfgEmployees;

import android.app.Service;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.view.ContextThemeWrapper;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.goflamingo.apps.gfgEmployees.model.CheckEmployee;
import com.goflamingo.apps.gfgEmployees.model.Employees;
import com.goflamingo.apps.gfgEmployees.network.GFGNetworkAPIService;
import com.goflamingo.apps.gfgEmployees.util.CacheManager;
import com.msg91.sendotp.library.SendOtpVerification;
import com.msg91.sendotp.library.Verification;
import com.msg91.sendotp.library.VerificationListener;

import org.json.JSONObject;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.goflamingo.apps.gfgEmployees.OtpActivity.mPleaseWaitDailoug;

public class MobileLoginActivity extends AppCompatActivity implements VerificationListener {

    private CacheManager mCachManager;
    private EditText mMobileNumber;
    private ProgressBar mProgressbar;
    public static Verification mVerification;
    private static Context context;

    public static void verifyOtp(Context context2, String otp) {
        context = context2;
        mPleaseWaitDailoug.setVisibility(View.VISIBLE);
        mVerification.verify(otp);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mobile_login);

        mCachManager = new CacheManager(this);
        mMobileNumber = findViewById(R.id.mobile_number);
        mProgressbar = findViewById(R.id.simpleProgressBar);
        mProgressbar.setVisibility(View.GONE);

        mMobileNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(s.length()==10){
                    InputMethodManager imm = (InputMethodManager) getSystemService(Service.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(mMobileNumber.getWindowToken(), 0);
                }
            }
            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }
    public void getOTP(View view) {
        if (validateMobile(mMobileNumber)) {

            isUserExist();
        }
    }
    public void isUserExist(){
        mProgressbar.setVisibility(View.VISIBLE);
        GFGNetworkAPIService.DeviceApi apiService = GFGNetworkAPIService.getInstance().newTransaction(this);
        Call<CheckEmployee> call = apiService.checkEmployee(mMobileNumber.getText().toString());
        call.enqueue(new Callback<CheckEmployee>() {
            @Override
            public void onResponse(Call<CheckEmployee> call, Response<CheckEmployee> response)
            {
                if(!call.isCanceled() && response.isSuccessful()){
                    mProgressbar.setVisibility(View.GONE);
                    if(response.body().getIs_preferred()){
                        mCachManager.setEmployeePk(response.body().getId());
                        sendMsgOTP(response.body().getOtp_sender_id(),response.body().getOtp_msg());
                    }else{
                        mProgressbar.setVisibility(View.GONE);
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.access_denied_contact_factory), Toast.LENGTH_LONG).show();
                    }
                }else{
                    mProgressbar.setVisibility(View.GONE);
                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        Toast.makeText(getApplicationContext(), jObjError.getString("detail"), Toast.LENGTH_LONG).show();
                    } catch (Exception e) {
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.please_try_again), Toast.LENGTH_LONG).show();
                        Log.i("Error", "Retrofit error" + e.getMessage());
                    }
                }

            }

            @Override
            public void onFailure(Call<CheckEmployee> call, Throwable t) {
                mProgressbar.setVisibility(View.GONE);
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.please_try_again), Toast.LENGTH_LONG).show();
                Log.i("error", "error" + t.getMessage());
            }
        });

    }
    private boolean validateMobile(EditText editText){
        if(editText.getText().toString().trim().length()!=10){
            editText.setError("please enter valid mobile number");
            editText.requestFocus();
        }
        return true;
    }
    public void sendMsgOTP(String sender_id,String otp_msg) {
        mVerification = SendOtpVerification.createSmsVerification
                (SendOtpVerification
                        .config("+91" + mMobileNumber.getText().toString())
                        .context(this)
                        .autoVerification(true)
                        .senderId(sender_id)
                        .message(otp_msg)
                        .build(), this);
        mVerification.initiate();
    }
    @Override
    public void onInitiated(String response) {
        Intent i = new Intent(this, OtpActivity.class);
        i.putExtra("mobile_num", mMobileNumber.getText().toString());
        startActivity(i);
        mProgressbar.setVisibility(View.GONE);
    }

    @Override
    public void onInitiationFailed(Exception paramException) {
        mProgressbar.setVisibility(View.GONE);
        Toast.makeText(getApplicationContext(), getResources().getString(R.string.wrong_otp) + paramException.getMessage(), Toast.LENGTH_LONG).show();
    }

    @Override
    public void onVerified(String response) {
        Toast.makeText(getApplicationContext(), getResources().getString(R.string.verified) + response.toString(), Toast.LENGTH_LONG).show();
        mCachManager.setIsFirstTimeLogin(false);
        Intent intent = new Intent(getApplicationContext(), SplashActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        mPleaseWaitDailoug.setVisibility(View.GONE);
    }

    @Override
    public void onVerificationFailed(Exception paramException) {

//        remove
//        onVerified("aaa");
        //remove
        mPleaseWaitDailoug.setVisibility(View.GONE);
        Toast.makeText(getApplicationContext(), getResources().getString(R.string.wrong_otp), Toast.LENGTH_LONG).show();
    }
    public void isNetworkAvailable(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        if (activeNetwork != null) {
            if (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI) {
            } else if (activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE) {
            }
        } else {
            new AlertDialog.Builder(new ContextThemeWrapper(MobileLoginActivity.this, R.style.AppTheme))
                    .setTitle(getResources().getString(R.string.internet_connection_error))
                    .setCancelable(true)
                    .setMessage(getResources().getString(R.string.internet_connection_error_msg))
                    .setNegativeButton(getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            finish();
                        }
                    }).setPositiveButton(getResources().getString(R.string.retry), new DialogInterface.OnClickListener() {
                @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            }).show();
        }
    }

}
