package com.goflamingo.apps.gfgEmployees;

import android.Manifest;
import android.app.Service;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.CountDownTimer;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.goflamingo.apps.gfgEmployees.util.CacheManager;

import java.util.ArrayList;
import java.util.List;

import static com.goflamingo.apps.gfgEmployees.MobileLoginActivity.mVerification;

public class OtpActivity extends AppCompatActivity {


    public static ProgressBar mPleaseWaitDailoug;
    private EditText mOtpEditText;
    private Button mSubmitButton;
    private TextView mRemainingTime, mMobileNumber;
    private LinearLayout mResendLayout;
    private CountDownTimer mTimer;
    private CacheManager CacheManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otp);

        mSubmitButton = findViewById(R.id.submit);
        mSubmitButton.setAlpha(.5f);
        mSubmitButton.setClickable(false);

        mRemainingTime = findViewById(R.id.remaining_time);
        mResendLayout = findViewById(R.id.resend_layout);
        mMobileNumber = findViewById(R.id.mobile_num);

        mOtpEditText = findViewById(R.id.otp_pin);
        CacheManager = new CacheManager(getApplicationContext());

        Intent i = getIntent();
        mMobileNumber.setText("to +91 " + i.getStringExtra("mobile_num"));

        mPleaseWaitDailoug = findViewById(R.id.simpleProgressBar);
        mPleaseWaitDailoug.setVisibility(View.GONE);

        mTimer = new CountDownTimer(30000, 1000) {
            public void onTick(long millisUntilFinished) {
                mRemainingTime.setText("" + millisUntilFinished / 1000 + " Sec");
            }

            public void onFinish() {
                mResendLayout.setVisibility(View.VISIBLE);
                mRemainingTime.setText(getResources().getString(R.string.done));
            }
        }.start();

        mOtpEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
            }
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // TODO Auto-generated method stub
            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() == 4) {
                    mSubmitButton.setAlpha(1f);
                    mSubmitButton.setClickable(true);
                    InputMethodManager imm = (InputMethodManager) getSystemService(Service.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(mMobileNumber.getWindowToken(), 0);
                } else {
                    mSubmitButton.setAlpha(0.5f);
                    mSubmitButton.setClickable(false);
                }
            }
        });

    }
    public void call(View view) {
        mVerification.resend(getResources().getString(R.string.voice));
    }

    public void verifyOTP(View view) {
        MobileLoginActivity.verifyOtp(OtpActivity.this, mOtpEditText.getText().toString());
    }



    public void resendOTP(View view) {
        mVerification.resend(getResources().getString(R.string.no_orders));
        mPleaseWaitDailoug.setVisibility(View.VISIBLE);
        finish();
    }

    public void callForOTP(View view) {
        mVerification.resend(getResources().getString(R.string.call));
        mPleaseWaitDailoug.setVisibility(View.VISIBLE);
        finish();
    }

    public void end_act() {
        finish();
    }

    private boolean checkAndRequestMsgPermissions() {
        int permissionSendMessage = ContextCompat.checkSelfPermission(this,
                Manifest.permission.SEND_SMS);

        int receiveSMS = ContextCompat.checkSelfPermission(this,
                Manifest.permission.RECEIVE_SMS);

        int readSMS = ContextCompat.checkSelfPermission(this,
                Manifest.permission.READ_SMS);
        List<String> listPermissionsNeeded = new ArrayList<>();

        if (receiveSMS != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.RECEIVE_MMS);
        }
        if (readSMS != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.READ_SMS);
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(this,
                    listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]),
                    CacheManager.REQUEST_ID_MULTIPLE_PERMISSIONS);
            return false;
        }
        return true;
    }

}
