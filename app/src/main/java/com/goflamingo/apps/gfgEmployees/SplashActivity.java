package com.goflamingo.apps.gfgEmployees;

import android.Manifest;
import android.app.Activity;
import android.app.AppComponentFactory;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.ContextThemeWrapper;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.goflamingo.apps.gfgEmployees.model.EmployeeConfig;
import com.goflamingo.apps.gfgEmployees.model.Tasks;
import com.goflamingo.apps.gfgEmployees.network.GFGNetworkAPIService;
import com.goflamingo.apps.gfgEmployees.util.CacheManager;
import com.goflamingo.apps.gfgEmployees.util.HideKeypad;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Method;
import java.net.HttpURLConnection;
import java.net.URL;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static butterknife.internal.Utils.arrayOf;

public class SplashActivity extends AppCompatActivity {
    private CacheManager mCacheManager;
    private String mVerNameApp;
    private String mVersionPath;
    TextView mVersionName;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_background);

        mVersionName = findViewById(R.id.app_version);

        mCacheManager = new CacheManager(this);
        PackageManager manager = getPackageManager();
        PackageInfo info = null;
        try {
            info = manager.getPackageInfo(this.getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        mVerNameApp = info.versionName;
        mVersionName.setText(mVerNameApp);

        compute();

    }
    public void compute(){

        if(mCacheManager.isFirstTimeLogin()){
            Intent i = new Intent(getApplicationContext(),MobileLoginActivity.class);
            startActivity(i);
            finish();

        }
        else{
            GFGNetworkAPIService.DeviceApi apiService = GFGNetworkAPIService
                    .getInstance().newTransaction(this);
            Call<EmployeeConfig> call = apiService.employeeConfg(mCacheManager.getEmployeepk());
            call.enqueue(new Callback<EmployeeConfig>() {
                @Override
                public void onResponse(Call<EmployeeConfig> call, Response<EmployeeConfig> response) {
                    if(!call.isCanceled() && response.isSuccessful()){

                        if(response.body().getUpdate().getApp_version().compareTo(mVerNameApp)>=1){
                            mVersionPath = response.body().getUpdate().getLink();
                            Log.i("vvvvvvvvvvvv","vvvvvvvvvvvvvvv"+mVersionName);

                            if(isStoragePermissionGranted()){
                                getUpdatePopUp();

                            }else{
                                ActivityCompat.requestPermissions(SplashActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                                getUpdatePopUp();
                            }
                        }else{
                            if(response.body().getEmployee().isIs_preferred()){

                                mCacheManager.setEmployeePk(response.body().getEmployee().getId());
                                mCacheManager.setEmployees(response.body().getOther_employees());
                                mCacheManager.setEmployeeName(response.body().getEmployee().getName());
                                mCacheManager.setPoints(response.body().getPoint());
                                mCacheManager.setPaginationSize(response.body().getUpdate().getPagination_count());

                                Intent intent = new Intent(SplashActivity.this, MainActivity.class);
                                startActivity(intent);
                                finish();
                            }
                        }
                    } else {
                        try {
                            // clearing app data
                            Runtime runtime = Runtime.getRuntime();
                            runtime.exec(getResources().getString(R.string.splash_msg));
                        } catch (Exception e) {
                            Log.i("Error clearing app", "");
                            e.printStackTrace();

                        }
                        //  startActivity(new Intent(getApplicationContext(),SignInActivity.class));
                        finish();
                    }

                }

                @Override
                public void onFailure(Call<EmployeeConfig> call, Throwable t) {
                    isNetworkAvailable(getApplicationContext());
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.please_try_again), Toast.LENGTH_LONG).show();
                    Log.i("error", "error" + t.getMessage());
                }
            });
        }

    }
    private void getUpdatePopUp() {


        Log.i("Update Pop Up","Update POP UP");
        new AlertDialog.Builder(SplashActivity.this)
                .setIcon(R.drawable.new_sutra3)
                .setTitle(getResources().getString(R.string.app_update))
                .setCancelable(false)
                .setPositiveButton(getResources().getString(R.string.update),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {


                                new DownloadFileFromURL().execute();
                            }
                        }
                )
                .setNegativeButton(R.string.cancel,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                finish();
                            }
                        }
                )
                .show();

    }
    public void isNetworkAvailable(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        if (activeNetwork != null) {
            if (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI) {
            } else if (activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE) {
            }
        } else {
            new AlertDialog.Builder(new ContextThemeWrapper(SplashActivity.this, R.style.AppTheme))
                    .setTitle(getResources().getString(R.string.internet_connection_error))
                    .setCancelable(true)
                    .setMessage(getResources().getString(R.string.internet_connection_error_msg))
                    .setNegativeButton(getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            finish();
                        }
                    }).setPositiveButton(getResources().getString(R.string.retry), new DialogInterface.OnClickListener() {
                @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            }).show();
        }
    }

    public static void setupUI(View view, final Context context) {
        if (!(view instanceof EditText)) {
            view.setOnTouchListener(new View.OnTouchListener() {
                public boolean onTouch(View v, MotionEvent event) {
                    HideKeypad.hideSoftKeyboard((Activity) context);
                    return false;
                }
            });
        }
    }

    private class DownloadFileFromURL extends AsyncTask<Void, Void, Void> {

        /**
         * Before starting background thread
         * Show Progress Bar Dialog
         */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        /**
         * Downloading file in background thread
         */
        @Override
        protected Void doInBackground(Void... f) {
            try {
                URL url = new URL(mVersionPath);
                HttpURLConnection c = (HttpURLConnection) url.openConnection();
                c.setRequestMethod("GET");
                c.connect();
                String PATH = Environment.getExternalStorageDirectory() + "/download/";
                File file = new File(PATH);
                file.mkdirs();
                File outputFile = new File(file, "g-one.apk");
                FileOutputStream fos = new FileOutputStream(outputFile);
                InputStream is = c.getInputStream();

                byte[] buffer = new byte[1024];
                int len1 = 0;
                while ((len1 = is.read(buffer)) != -1) {
                    fos.write(buffer, 0, len1);
                }
                fos.close();
                is.close();
            } catch (IOException e) {
                Log.e("Error","Error="+e);
            }
            return null;
        }

        /**
         * Updating progress bar
         */

        @Override
        protected void onPostExecute(Void f) {


            super.onPostExecute(f);

            // dismiss the dialog after the file was downloaded

            File file = new File(Environment.getExternalStorageDirectory() + "/download/" + "g-one.apk");


            Intent intent = new Intent(Intent.ACTION_VIEW);
            String type = "application/vnd.android.package-archive";

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                Uri downloadedApk = FileProvider.getUriForFile(SplashActivity.this, getApplicationContext().getPackageName() , file);

                intent.setDataAndType(downloadedApk, type);
                intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            } else {
                intent.setDataAndType(Uri.fromFile(file), type);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            }

            SplashActivity.this.startActivity(intent);
        }
    }

    public  boolean isStoragePermissionGranted() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED) {
                Log.v("Eror","Permission is granted");
                return true;
            } else {

                Log.v("Error","Permission is revoked");
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                return false;
            }
        }
        else { //permission is automatically granted on sdk<23 upon installation
            Log.v("Error","Permission is granted");
            return true;
        }
    }

}

