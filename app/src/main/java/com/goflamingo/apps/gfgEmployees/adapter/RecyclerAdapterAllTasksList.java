package com.goflamingo.apps.gfgEmployees.adapter;

import android.content.Context;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.goflamingo.apps.gfgEmployees.R;
import com.goflamingo.apps.gfgEmployees.fragments.DoneFragment;
import com.goflamingo.apps.gfgEmployees.model.AllTasks;
import com.goflamingo.apps.gfgEmployees.model.Tasks;
import com.goflamingo.apps.gfgEmployees.network.GFGNetworkAPIService;
import com.goflamingo.apps.gfgEmployees.util.CacheManager;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RecyclerAdapterAllTasksList extends RecyclerView.Adapter<RecyclerAdapterAllTasksList.RecyclerViewHolder> {
    private Context context;
    private CacheManager mCachemanager;
    private LayoutInflater inflater;
    private ArrayList<AllTasks> data;
    private View v;
    public RecyclerAdapterAllTasksList(Context context) {
        this.context = context;
        mCachemanager = new CacheManager(context);
        inflater = LayoutInflater.from(context);
    }
    public void setSchedule(ArrayList<AllTasks> data) {
        this.data = data;
    }
    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int i) {

        v = inflater.inflate(R.layout.single_row_regualr_tasks, parent, false);
        RecyclerAdapterAllTasksList.RecyclerViewHolder viewHolder = new RecyclerAdapterAllTasksList.RecyclerViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final RecyclerViewHolder holder, int i) {

        holder.mTask.setText(data.get(i).getDesc());
        holder.mDate.setText(data.get(i).getAssigned_date());
        holder.mFrom.setText("Created by :"+data.get(i).getFrom_emp().getName());
        holder.mTO.setText("Assigned to :"+data.get(i).getTo_emp().getName());
        holder.mStatus.setText(data.get(i).getStatus());
        holder.mPriority.setText(data.get(i).getPoint().getName());

        if(data.get(i).getTo_emp().getId().equals("23")){
            holder.mStartTaskBtn.setVisibility(View.VISIBLE);
        }else{
            holder.mStartTaskBtn.setVisibility(View.GONE);
        }


        if(data.get(i).getStatus().equals("Done")){
            holder.mStatus.setTextColor(context.getResources().getColor(R.color.green));
            holder.mStartTaskBtn.setVisibility(View.GONE);
        }else if(data.get(i).getStatus().equals("Doing")){
            holder.mStatus.setTextColor(context.getResources().getColor(R.color.red));
            holder.mStartTaskBtn.setVisibility(View.GONE);
        }else if(data.get(i).getStatus().equals("ToDo")){
            holder.mStatus.setTextColor(context.getResources().getColor(R.color.colorPrimary));
        }else{
            holder.mPriority.setTextColor(context.getResources().getColor(R.color.black));
            holder.mStartTaskBtn.setVisibility(View.GONE);
        }

        if(data.get(i).getPoint().getName().equals("Regular")){
          holder.mPriority.setTextColor(context.getResources().getColor(R.color.chocolate));
        }else if(data.get(i).getPoint().getName().equals("Important")){
            holder.mPriority.setTextColor(context.getResources().getColor(R.color.approved));
        }else if(data.get(i).getPoint().getName().equals("Critical")){
            holder.mPriority.setTextColor(context.getResources().getColor(R.color.red));
        }else{

        }

        View.OnClickListener mTaskStart = new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onClick(View view) {

                RecyclerAdapterAllTasksList.RecyclerViewHolder vholder = (RecyclerViewHolder) view.getTag();
                final int position = vholder.getPosition();
                holder.taskUpdate("Doing",data.get(position).getId(),position);
            }
        };

        holder.mStartTaskBtn.setOnClickListener(mTaskStart);
        holder.mStartTaskBtn.setTag(holder);

        try {

            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
            sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
            String data2 = data.get(i).getAssigned_date().substring(0, 19) + ".000Z";
            Date date2 = sdf.parse(data2);
            SimpleDateFormat outputFormat = new SimpleDateFormat("dd-MM-yyyy  hh:mm a", Locale.ENGLISH);
            outputFormat.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"));
            holder.mDate.setText(outputFormat.format(date2));
        } catch (Exception e) {

        }
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class RecyclerViewHolder extends RecyclerView.ViewHolder {

        private TextView mFrom,mDate,mTask,mTO,mStatus,mPriority,mStartTaskBtn;
        private ProgressBar mEndPogress;
        public RecyclerViewHolder(@NonNull View itemView) {
            super(itemView);

            mFrom = itemView.findViewById(R.id.regular_from);
            mDate = itemView.findViewById(R.id.date);
            mTask = itemView.findViewById(R.id.tasks_id);
            mStartTaskBtn = itemView.findViewById(R.id.start_task);
            mEndPogress = itemView.findViewById(R.id.progressbar_start);
            mTO = itemView.findViewById(R.id.to_from);
            mStatus = itemView.findViewById(R.id.status);
            mPriority  = itemView.findViewById(R.id.priority);

        }

        public void taskUpdate(final String status, String id, final int position) {
            mEndPogress.setVisibility(View.VISIBLE);
            GFGNetworkAPIService.DeviceApi apiService = GFGNetworkAPIService.getInstance().newTransaction(context);
            Call<AllTasks> call = apiService.postEndStatus(id, status, mCachemanager.getEmployeepk(),"");
            call.enqueue(new Callback<AllTasks>() {
                @Override
                public void onResponse(Call<AllTasks> call, Response<AllTasks> response) {
                    if(!call.isCanceled() && response.isSuccessful()){
                        mEndPogress.setVisibility(View.GONE);
                        Toast.makeText(context.getApplicationContext(),status,Toast.LENGTH_LONG).show();
                        data.remove(position);
                        data.add(position,response.body());
                        notifyDataSetChanged();
                    }else{
                        try {
                            JSONObject jObjError = new JSONObject(response.errorBody().string());
                            Toast.makeText(context.getApplicationContext(), jObjError.getString("detail"), Toast.LENGTH_LONG).show();
                        } catch (Exception e) {
                            Toast.makeText(context.getApplicationContext(), context.getResources().getString(R.string.please_try_again), Toast.LENGTH_LONG).show();
                            Log.i("Error", "Retrofit error" + e.getMessage());
                        }
                    }

                }

                @Override
                public void onFailure(Call<AllTasks> call, Throwable t) {
                    mEndPogress.setVisibility(View.GONE);
                    Log.i("error", "error" + t.getMessage());
                    Toast.makeText(context.getApplicationContext(), context.getResources().getString(R.string.please_try_again), Toast.LENGTH_LONG).show();
                }
            });
        }

    }
}
