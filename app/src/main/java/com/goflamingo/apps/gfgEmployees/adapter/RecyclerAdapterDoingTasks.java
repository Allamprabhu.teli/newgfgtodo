package com.goflamingo.apps.gfgEmployees.adapter;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.goflamingo.apps.gfgEmployees.R;
import com.goflamingo.apps.gfgEmployees.model.AllTasks;
import com.goflamingo.apps.gfgEmployees.model.Employees;
import com.goflamingo.apps.gfgEmployees.model.Tasks;
import com.goflamingo.apps.gfgEmployees.network.GFGNetworkAPIService;
import com.goflamingo.apps.gfgEmployees.util.CacheManager;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RecyclerAdapterDoingTasks extends RecyclerView.Adapter<RecyclerAdapterDoingTasks.RecyclerViewHolder> {

    private Context context;
    private CacheManager mCachemanager;
    private LayoutInflater inflater;
    private ArrayList<AllTasks> data;
    private String high_five_pk="";
    private View v;
    public RecyclerAdapterDoingTasks(Context context) {
        this.context = context;
        mCachemanager = new CacheManager(context);
        inflater = LayoutInflater.from(context);
    }
    public void setSchedule(ArrayList<AllTasks> data) {
        this.data = data;

    }
    @Override
    public RecyclerAdapterDoingTasks.RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int i) {
        v = inflater.inflate(R.layout.single_row_doing_tasks, parent, false);
        RecyclerAdapterDoingTasks.RecyclerViewHolder viewHolder = new RecyclerAdapterDoingTasks.RecyclerViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final RecyclerAdapterDoingTasks.RecyclerViewHolder holder, int i) {
        holder.mTask.setText(data.get(i).getDesc());
        holder.mDate.setText(data.get(i).getAssigned_date());
        holder.mFrom.setText(data.get(i).getFrom_emp().getName());

        View.OnClickListener taskStart = new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onClick(View view) {

                RecyclerAdapterDoingTasks.RecyclerViewHolder vholder = (RecyclerAdapterDoingTasks.RecyclerViewHolder) view.getTag();
                final int position = vholder.getPosition();

                holder.showDailoug("Done",data.get(position).getId(),position).show();

            }
        };

        holder.mEndButton.setOnClickListener(taskStart);
        holder.mEndButton.setTag(holder);

        holder.mPriority.setText(data.get(i).getPoint().getName());

        if(data.get(i).getPoint().getName().equals("Regular")){
            holder.mPriority.setTextColor(context.getResources().getColor(R.color.chocolate));
        }else if(data.get(i).getPoint().getName().equals("Important")){
            holder.mPriority.setTextColor(context.getResources().getColor(R.color.approved));
        }else if(data.get(i).getPoint().getName().equals("Critical")){
            holder.mPriority.setTextColor(context.getResources().getColor(R.color.red));
        }



        try {

            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
            sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
            String data2 = data.get(i).getAssigned_date().substring(0, 19) + ".000Z";
            Date date2 = sdf.parse(data2);
            SimpleDateFormat outputFormat = new SimpleDateFormat("dd-MM-yyyy  hh:mm a", Locale.ENGLISH);
            outputFormat.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"));
            holder.mDate.setText(outputFormat.format(date2));
        } catch (Exception e) {

        }

    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class RecyclerViewHolder extends RecyclerView.ViewHolder {
            private TextView mFrom,mDate,mTask,mPriority,mEndButton;

        private ProgressBar mProgressbar;
        public RecyclerViewHolder(View itemView) {
            super(itemView);
            mFrom = itemView.findViewById(R.id.doing_from);
            mDate = itemView.findViewById(R.id.date);
            mTask = itemView.findViewById(R.id.doing_tasks);
            mEndButton = itemView.findViewById(R.id.end_btn);
            mProgressbar = itemView.findViewById(R.id.progressbar_end);
            mPriority = itemView.findViewById(R.id.priority);
        }

        public void taskUpdate(final String status, String id, final int position,String high_five) {
            mProgressbar.setVisibility(View.VISIBLE);
            GFGNetworkAPIService.DeviceApi apiService = GFGNetworkAPIService.getInstance().newTransaction(context);
            Call<AllTasks> call = apiService.postEndStatus(id, status, mCachemanager.getEmployeepk(),high_five);
            call.enqueue(new Callback<AllTasks>() {
                @Override
                public void onResponse(Call<AllTasks> call, Response<AllTasks> response) {
                    if(!call.isCanceled() && response.isSuccessful()){
                        mProgressbar.setVisibility(View.GONE);
                        Toast.makeText(context.getApplicationContext(),status,Toast.LENGTH_LONG).show();
                        data.remove(position);
                        notifyDataSetChanged();
                    }else{
                        try {
                            JSONObject jObjError = new JSONObject(response.errorBody().string());
                            Toast.makeText(context.getApplicationContext(), jObjError.getString("detail"), Toast.LENGTH_LONG).show();
                        } catch (Exception e) {
                            Toast.makeText(context.getApplicationContext(), context.getResources().getString(R.string.please_try_again), Toast.LENGTH_LONG).show();
                            Log.i("Error", "Retrofit error" + e.getMessage());
                        }
                    }

                }

                @Override
                public void onFailure(Call<AllTasks> call, Throwable t) {
                    mProgressbar.setVisibility(View.GONE);
                    Log.i("error", "error" + t.getMessage());
                    Toast.makeText(context.getApplicationContext(), context.getResources().getString(R.string.please_try_again), Toast.LENGTH_LONG).show();
                }
            });
        }


        public android.support.v7.app.AlertDialog showDailoug(final String status, final String task_id, final int position) {

            final LayoutInflater layoutInflaterAndroid = LayoutInflater.from(context);
            View mView = layoutInflaterAndroid.inflate(R.layout.user_input_dialog_box, null);
            android.support.v7.app.AlertDialog.Builder alertDialogBuilderUserInput = new android.support.v7.app.AlertDialog.Builder(context);
            alertDialogBuilderUserInput.setView(mView);

            final Spinner mEmployees = (Spinner) mView.findViewById(R.id.store_house_spinner);


            ArrayList<String> store_house = new ArrayList<String>();
            store_house.add("No One");
            for (int i = 0; i < mCachemanager.getEmployees().size(); i++) {
                store_house.add(mCachemanager.getEmployees().get(i).getName());
            }
            // Creating adapter for spinner
            ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(context, android.R.layout.simple_spinner_item, store_house);
            // Drop down layout style - list view with radio button
            dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            // attaching data adapter to spinner
            mEmployees.setAdapter(dataAdapter);
            mEmployees.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    Log.i("seeeee","seeeeeeeeeeeeeeeeeeeeeeeeeee"+mEmployees.getSelectedItem());
                    high_five_pk = "";
                    for(int j=0;j<mCachemanager.getEmployees().size();j++){
                        if(mEmployees.getSelectedItem().equals(mCachemanager.getEmployees().get(j).getName())){
                            high_five_pk = mCachemanager.getEmployees().get(j).getId();
                        }
                    }
                }
                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                }
            });
            alertDialogBuilderUserInput
                    .setCancelable(false)
                    .setPositiveButton("Finish", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialogBox, int id) {
                            // ToDo get user input here
                        }
                    })
                    .setNegativeButton(R.string.cancel,
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialogBox, int id) {
                                    dialogBox.cancel();
                                }
                            });

            final android.support.v7.app.AlertDialog alertDialogAndroid = alertDialogBuilderUserInput.create();
            alertDialogAndroid.setOnShowListener(new DialogInterface.OnShowListener() {
                @Override
                public void onShow(DialogInterface dialogInterface) {
                    Button button = ((android.support.v7.app.AlertDialog) alertDialogAndroid).getButton(android.support.v7.app.AlertDialog.BUTTON_POSITIVE);
                    button.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            alertDialogAndroid.dismiss();
                            taskUpdate("Done",task_id,position,high_five_pk);
                        }
                    });
                }
            });
            return alertDialogAndroid;
        }
    }
}
