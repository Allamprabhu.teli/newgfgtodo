package com.goflamingo.apps.gfgEmployees.adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.goflamingo.apps.gfgEmployees.R;
import com.goflamingo.apps.gfgEmployees.model.AllTasks;
import com.goflamingo.apps.gfgEmployees.model.Employees;
import com.goflamingo.apps.gfgEmployees.model.Tasks;
import com.goflamingo.apps.gfgEmployees.network.GFGNetworkAPIService;
import com.goflamingo.apps.gfgEmployees.util.CacheManager;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RecyclerAdapterToDoTasks extends RecyclerView.Adapter<RecyclerAdapterToDoTasks.RecyclerViewHolder> {

    private Context context;
    private CacheManager mCachemanager;
    private LayoutInflater inflater;
    private ArrayList<AllTasks> data;
    private View v;
    private String mEmployee_pk,mEmployeeName;
    public RecyclerAdapterToDoTasks(Context context) {
        this.context = context;
        mCachemanager = new CacheManager(context);
        inflater = LayoutInflater.from(context);
    }
    public void setSchedule(ArrayList<AllTasks> data) {
        this.data = data;

    }

    @Override
    public RecyclerAdapterToDoTasks.RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int i) {
        v = inflater.inflate(R.layout.single_row_one_time_task, parent, false);
        RecyclerAdapterToDoTasks.RecyclerViewHolder viewHolder = new RecyclerAdapterToDoTasks.RecyclerViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final RecyclerAdapterToDoTasks.RecyclerViewHolder holder, int i) {

        holder.mTodoTasks.setText(data.get(i).getDesc());
        holder.mDate.setText(data.get(i).getAssigned_date());
        holder.mFrom.setText("Created by :"+data.get(i).getFrom_emp().getName());

        View.OnClickListener taskStart = new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onClick(View view) {
                    RecyclerAdapterToDoTasks.RecyclerViewHolder vholder = (RecyclerAdapterToDoTasks.RecyclerViewHolder) view.getTag();
                    final int position = vholder.getPosition();
                if(view.equals(holder.mStartTaskButton)) {
                    holder.taskUpdate("Doing", data.get(position).getId(), position);
                }else if(view.equals(holder.mReassignTask)){
                    holder.showDialog("ToDo",data.get(position).getId(),position).show();
                }
            }
        };
        holder.mStartTaskButton.setOnClickListener(taskStart);
        holder.mStartTaskButton.setTag(holder);
        holder.mReassignTask.setOnClickListener(taskStart);
        holder.mReassignTask.setTag(holder);

        holder.mPriority.setText(data.get(i).getPoint().getName());

        if(data.get(i).getPoint().getName().equals("Regular")){
            holder.mPriority.setTextColor(context.getResources().getColor(R.color.chocolate));
        }else if(data.get(i).getPoint().getName().equals("Important")){
            holder.mPriority.setTextColor(context.getResources().getColor(R.color.approved));
        }else if(data.get(i).getPoint().getName().equals("Critical")){
            holder.mPriority.setTextColor(context.getResources().getColor(R.color.red));
        }


        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
            sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
            String data2 = data.get(i).getAssigned_date().substring(0, 19) + ".000Z";
            Date date2 = sdf.parse(data2);
            SimpleDateFormat outputFormat = new SimpleDateFormat("dd-MM-yyyy  hh:mm a", Locale.ENGLISH);
            outputFormat.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"));
            holder.mDate.setText(outputFormat.format(date2));
        } catch (Exception e) {

        }

    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class RecyclerViewHolder extends RecyclerView.ViewHolder {
        private TextView mFrom,mDate,mTodoTasks,mPriority,mStartTaskButton,mReassignTask;
        private ProgressBar mEndPogress,mReassignProgressbar;
        public RecyclerViewHolder( View itemView) {
            super(itemView);

            mFrom = itemView.findViewById(R.id.todo_from);
            mDate = itemView.findViewById(R.id.date);
            mTodoTasks = itemView.findViewById(R.id.tasks_id);
            mEndPogress = itemView.findViewById(R.id.progressbar_start);
            mStartTaskButton = itemView.findViewById(R.id.start_task);
            mPriority = itemView.findViewById(R.id.priority);
            mReassignTask  = itemView.findViewById(R.id.task_reassign);
            mReassignProgressbar = itemView.findViewById(R.id.progressbar_reassign);

        }

        public void taskUpdate(final String status, String id, final int position) {
            mEndPogress.setVisibility(View.VISIBLE);
            GFGNetworkAPIService.DeviceApi apiService = GFGNetworkAPIService.getInstance().newTransaction(context);
            Call<AllTasks> call = apiService.postEndStatus(id, status, mCachemanager.getEmployeepk(),"");
            call.enqueue(new Callback<AllTasks>() {
                @Override
                public void onResponse(Call<AllTasks> call, Response<AllTasks> response) {
                    mEndPogress.setVisibility(View.GONE);
                    if(!call.isCanceled() && response.isSuccessful()){
                        Toast.makeText(context.getApplicationContext(),status,Toast.LENGTH_LONG).show();
                        data.remove(position);
                        notifyDataSetChanged();
                    }else{
                        try {
                            JSONObject jObjError = new JSONObject(response.errorBody().string());
                            Toast.makeText(context.getApplicationContext(), jObjError.getString("detail"), Toast.LENGTH_LONG).show();
                        } catch (Exception e) {
                            Toast.makeText(context.getApplicationContext(), context.getResources().getString(R.string.please_try_again), Toast.LENGTH_LONG).show();
                            Log.i("Error", "Retrofit error" + e.getMessage());
                        }
                    }

                }

                @Override
                public void onFailure(Call<AllTasks> call, Throwable t) {
                    mEndPogress.setVisibility(View.GONE);
                    Log.i("error", "error" + t.getMessage());
                    Toast.makeText(context.getApplicationContext(), context.getResources().getString(R.string.please_try_again), Toast.LENGTH_LONG).show();
                }
            });
        }
        public android.support.v7.app.AlertDialog showDialog(String status,final String task_id,final int position){
            final LayoutInflater layoutInflaterAndroid = LayoutInflater.from(context);
            View mView = layoutInflaterAndroid.inflate(R.layout.user_input_dialog, null);
            android.support.v7.app.AlertDialog.Builder alertDialogBuilderUserInput = new android.support.v7.app.AlertDialog.Builder(context);
            alertDialogBuilderUserInput.setView(mView);

            final Spinner mEmployees = (Spinner) mView.findViewById(R.id.employee_spinner);
            ArrayList<String> EmployeeList = new ArrayList<String>();
            for (int i = 0; i < mCachemanager.getEmployees().size(); i++) {
                EmployeeList.add(mCachemanager.getEmployees().get(i).getName());
            }
            // Creating adapter for spinner
            ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(context, android.R.layout.simple_spinner_item, EmployeeList);
            // Drop down layout style - list view with radio button
            dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            // attaching data adapter to spinner
            mEmployees.setAdapter(dataAdapter);
            mEmployees.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    mEmployee_pk = "";
                    for(int i=0;i<mCachemanager.getEmployees().size();i++){

                        if(mEmployees.getSelectedItem().equals(mCachemanager.getEmployees().get(i).getName())){
                            mEmployee_pk = mCachemanager.getEmployees().get(i).getId();
                            mEmployeeName = mCachemanager.getEmployees().get(i).getName();
                        }
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
            alertDialogBuilderUserInput
                    .setCancelable(false)
                    .setTitle("Reassign to")
                    .setPositiveButton("ok", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialogBox, int id) {
                            // ToDo get user input here
                        }
                    })
                    .setNegativeButton(R.string.cancel,
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialogBox, int id) {
                                    dialogBox.cancel();
                                }
                            });
            final android.support.v7.app.AlertDialog alertDialogAndroid = alertDialogBuilderUserInput.create();
            alertDialogAndroid.setOnShowListener(new DialogInterface.OnShowListener() {
                @Override
                public void onShow(DialogInterface dialogInterface) {
                    Button button = ((android.support.v7.app.AlertDialog) alertDialogAndroid).getButton(android.support.v7.app.AlertDialog.BUTTON_POSITIVE);
                    button.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            alertDialogAndroid.dismiss();
                            taskReassign("ToDo",task_id,position,mEmployee_pk);
                        }
                    });
                }
            });
            return alertDialogAndroid;

        }
        public  void taskReassign(String status,String task_id,final int position,String mEmployee_pk){
            mReassignProgressbar.setVisibility(View.VISIBLE);
            GFGNetworkAPIService.DeviceApi apiService = GFGNetworkAPIService.getInstance().newTransaction(context);
            Call<Tasks> call = apiService.reassignToSomeOne(task_id, mCachemanager.getEmployeepk(), mEmployee_pk, status);
            call.enqueue(new Callback<Tasks>() {
                @Override
                public void onResponse(Call<Tasks> call, Response<Tasks> response) {
                    mReassignProgressbar.setVisibility(View.GONE);
                    if(!call.isCanceled()&&response.isSuccessful()){
                        data.remove(position);
                        notifyDataSetChanged();
                        Toast.makeText(context.getApplicationContext(), "Reassigned to"+" "+mEmployeeName, Toast.LENGTH_SHORT).show();

                    }else{
                        try {
                            JSONObject jObjError = new JSONObject(response.errorBody().string());
                            Toast.makeText(context.getApplicationContext(), jObjError.getString("detail"), Toast.LENGTH_LONG).show();
                        } catch (Exception e) {
                            Toast.makeText(context.getApplicationContext(), context.getResources().getString(R.string.please_try_again), Toast.LENGTH_LONG).show();
                            Log.i("Error", "Retrofit error" + e.getMessage());
                        }
                    }
                }
                @Override
                public void onFailure(Call<Tasks> call, Throwable t) {
                    mReassignProgressbar.setVisibility(View.GONE);
                    Log.i("error", "error" + t.getMessage());
                    Toast.makeText(context.getApplicationContext(), context.getResources().getString(R.string.please_try_again), Toast.LENGTH_SHORT).show();
                }
            });

        }
    }
}

