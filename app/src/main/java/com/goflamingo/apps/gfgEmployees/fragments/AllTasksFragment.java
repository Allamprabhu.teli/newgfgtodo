package com.goflamingo.apps.gfgEmployees.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v4.app.FragmentActivity;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.Toast;

import com.goflamingo.apps.gfgEmployees.AddNewTaskActivity;
import com.goflamingo.apps.gfgEmployees.MainActivity;
import com.goflamingo.apps.gfgEmployees.R;
import com.goflamingo.apps.gfgEmployees.adapter.RecyclerAdapterAllTasksList;
import com.goflamingo.apps.gfgEmployees.model.AllTasks;
import com.goflamingo.apps.gfgEmployees.model.CheckEmployee;
import com.goflamingo.apps.gfgEmployees.model.Tasks;
import com.goflamingo.apps.gfgEmployees.network.GFGNetworkAPIService;
import com.goflamingo.apps.gfgEmployees.util.CacheManager;
import com.goflamingo.apps.gfgEmployees.util.EmptyMsgView;
import com.goflamingo.apps.gfgEmployees.util.PaginationCustom;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link AllTasksFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link AllTasksFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class AllTasksFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    public static String all_count="0";

    public int finalMTotalItemsCount = 0;
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private View view;
    public List<String> mEmployees;
    private CacheManager mCachmanger;
    private RecyclerView mRecyclerView;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private LinearLayoutManager mLinerLayoutManager;
    private ProgressBar mProgressbar;
    private FragmentActivity listener;
    RecyclerAdapterAllTasksList madapter;
    private ArrayList<AllTasks> mTasksList;
    private boolean isPageLoading = false;
    private OnFragmentInteractionListener mListener;
    private EmptyMsgView mEmptyMsgView;
    private Spinner mEmployeeSpinner;
    private String mEmployeeId="";
    private View spinnerView;


    public AllTasksFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment AllTasksFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static AllTasksFragment newInstance(String param1, String param2) {
        AllTasksFragment fragment = new AllTasksFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
            setHasOptionsMenu(true);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_all_tasks, container, false);
        setHasOptionsMenu(true);

        mProgressbar = view.findViewById(R.id.progressbar);
        mProgressbar.setVisibility(View.GONE);
        mCachmanger = new CacheManager(getActivity());
        mEmptyMsgView = new EmptyMsgView(getActivity(),view.findViewById(R.id.all_tasks_fragment_layout),"No tasks are available");

        mRecyclerView = view.findViewById(R.id.recyclerview);
        mSwipeRefreshLayout = view.findViewById(R.id.allTasks_swipe);
        mSwipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
            allTassksList(1,mEmployeeId);
            }
        });
        listener.setTitle(getResources().getString(R.string.all_tasks_toolbar));
        allTassksList(1,mEmployeeId);

        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof Activity) {
           this.listener = (FragmentActivity) context;
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_filter:
                filteredEmployees();
                break;
            default:
                break;
        }
        return true;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        getActivity().getMenuInflater().inflate(R.menu.paymentfilter_logo,menu);

    }

    //seting the employees in spinner
    public void filteredEmployees() {

        mEmployees = new ArrayList<String>();
        for (int i = 0; i < mCachmanger.getEmployees().size(); i++) {
            mEmployees.add(mCachmanger.getEmployees().get(i).getName());
        }
        spinnerView  = getActivity().getLayoutInflater().inflate(R.layout.user_input_dialog, null);
        mEmployeeSpinner = spinnerView.findViewById(R.id.employee_spinner);
        ArrayAdapter adapter = new ArrayAdapter(getActivity().getApplicationContext(), android.R.layout.simple_spinner_item, mEmployees);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mEmployeeSpinner.setAdapter(adapter);
        MainActivity.builder.setTitle("Filter by employee");
        MainActivity.builder.setPositiveButton(getResources().getString(R.string.apply), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                long position = mEmployeeSpinner.getSelectedItemId();
                int i = (int) position;
                mEmployeeId = mCachmanger.getEmployees().get(i).getId();
                 Log.i("idddddddddd","dddddddd"+mEmployeeId+ " "+mCachmanger.getEmployees().get(i).getName());
                allTassksList(1,mEmployeeId);
                dialog.dismiss();
            }
        });
        MainActivity.builder.setView(spinnerView);
        MainActivity.builder.show();
    }

    private void allTassksList(final int page,String to_emp) {
        mProgressbar.setVisibility(View.VISIBLE);
        mEmptyMsgView.invisible();
        GFGNetworkAPIService.DeviceApi apiService = GFGNetworkAPIService
                .getInstance().newTransaction(getActivity().getApplicationContext());
        Call<Tasks> call = apiService.getAllTasks(page,to_emp);
        call.enqueue(new Callback<Tasks>() {
            @Override
            public void onResponse(Call<Tasks> call, Response<Tasks> response) {
                if(!call.isCanceled() && response.isSuccessful()){
                    all_count = response.body().getCount();
                    if(!response.body().getCount().equals("0")) {
                        MainActivity.menu.getItem(3).setTitle("All(" + response.body().getCount() + ")");
                    }else{
                        MainActivity.menu.getItem(3).setTitle("All");
                    }
                    mProgressbar.setVisibility(View.GONE);
                    mSwipeRefreshLayout.setRefreshing(false);

                    if(page==1) {
                        if(response.body().getResults().isEmpty()){
                            mEmptyMsgView.visible();
                            mSwipeRefreshLayout.setRefreshing(false);
                        }

                            finalMTotalItemsCount = Integer.parseInt(response.body().getCount());
                        mTasksList = response.body().getResults();

                        mLinerLayoutManager = new LinearLayoutManager(getActivity());
                        mLinerLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
                        try {

                            mRecyclerView = view.findViewById(R.id.recyclerview);
                            madapter = new RecyclerAdapterAllTasksList(getActivity());
                            madapter.setSchedule(mTasksList);
                            mRecyclerView.setAdapter(madapter);
                            mRecyclerView.setHasFixedSize(true);
                            mRecyclerView.setLayoutManager(mLinerLayoutManager);
                            mRecyclerView.setNestedScrollingEnabled(false);
                        } catch (Exception e) {

                        }
                       mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                           @Override
                           public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                               super.onScrollStateChanged(recyclerView, newState);
                           }

                           @Override
                           public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                               super.onScrolled(recyclerView, dx, dy);
                               if(dy>0){
                                   if ((!isPageLoading && mLinerLayoutManager.findLastCompletelyVisibleItemPosition() > mTasksList.size() - 5)
                                           && mTasksList.size() < finalMTotalItemsCount) {
                                       isPageLoading = true;
                                       int mPageIndex = PaginationCustom.returnPageIndex(mTasksList.size(), mCachmanger.getPaginationSize());
                                       allTassksList(mPageIndex,mEmployeeId);
                                   }
                               }
                           }
                       });
                    }else{
                        mTasksList.addAll(response.body().getResults());
                        madapter.setSchedule(mTasksList);
                        madapter.notifyDataSetChanged();
                        isPageLoading = false;
                    }
                }else {
                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        Toast.makeText(getActivity().getApplicationContext(), jObjError.getString("detail"), Toast.LENGTH_LONG).show();
                    } catch (Exception e) {
                        Toast.makeText(getActivity().getApplicationContext(), getResources().getString(R.string.please_try_again), Toast.LENGTH_LONG).show();
                        Log.i("Error", "Retrofit error" + e.getMessage());
                    }
                }

            }

            @Override
            public void onFailure(Call<Tasks> call, Throwable t) {
                mProgressbar.setVisibility(View.GONE);
                mSwipeRefreshLayout.setRefreshing(false);
                Log.i("error", "error" + t.getMessage());
                Toast.makeText(getActivity().getApplicationContext(), getResources().getString(R.string.please_try_again), Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void addNewTask(View view) {
        Intent i = new Intent(getActivity().getApplicationContext(), AddNewTaskActivity.class);
        startActivity(i);
        getActivity().finish();

    }
}