package com.goflamingo.apps.gfgEmployees.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.app.FragmentActivity;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.goflamingo.apps.gfgEmployees.AddNewTaskActivity;
import com.goflamingo.apps.gfgEmployees.MainActivity;
import com.goflamingo.apps.gfgEmployees.R;
import com.goflamingo.apps.gfgEmployees.adapter.RecyclerAdapterAllTasksList;
import com.goflamingo.apps.gfgEmployees.adapter.RecyclerAdapterDoingTasks;
import com.goflamingo.apps.gfgEmployees.model.CheckEmployee;
import com.goflamingo.apps.gfgEmployees.model.Tasks;
import com.goflamingo.apps.gfgEmployees.network.GFGNetworkAPIService;
import com.goflamingo.apps.gfgEmployees.util.CacheManager;
import com.goflamingo.apps.gfgEmployees.util.EmptyMsgView;

import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link AllTasksFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link AllTasksFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class DoingFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    public static String doing_count="0";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private View view;
    private CacheManager mCachmanger;
    private RecyclerView mRecyclerView;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private LinearLayoutManager mLinerLayoutManager;
    private ProgressBar mProgressbar;
    private FragmentActivity listener;
    RecyclerAdapterDoingTasks madapter;
    private OnFragmentInteractionListener mListener;
    private EmptyMsgView mEmptyMsgView;

    public DoingFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment AllTasksFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static DoingFragment newInstance(String param1, String param2) {
        DoingFragment fragment = new DoingFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_doing, container, false);

        mProgressbar = view.findViewById(R.id.progressBar);
        mProgressbar.setVisibility(View.GONE);
        mCachmanger = new CacheManager(getActivity());
        mRecyclerView = view.findViewById(R.id.recyclerview);
        mEmptyMsgView = new EmptyMsgView(getActivity(),view.findViewById(R.id.doing_tasks_layout),"No Doing tasks are available");
        mSwipeRefreshLayout = view.findViewById(R.id.doing_swipe);
        mSwipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                allDoingTasks();
            }
        });
//        listener.setTitle(getResources().getString(R.string.tenders_toolbar));
        allDoingTasks();

        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof Activity) {
            this.listener = (FragmentActivity) context;
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    private void allDoingTasks() {
        mProgressbar.setVisibility(View.VISIBLE);
        mEmptyMsgView.invisible();
        GFGNetworkAPIService.DeviceApi apiService = GFGNetworkAPIService
                .getInstance().newTransaction(getActivity().getApplicationContext());
        Call<Tasks> call = apiService.getDoingTasks("Doing",mCachmanger.getEmployeepk());
        call.enqueue(new Callback<Tasks>() {
            @Override
            public void onResponse(Call<Tasks> call, Response<Tasks> response) {
                if(!call.isCanceled() && response.isSuccessful()){

                    if(!response.body().getCount().equals("0")) {
                        MainActivity.menu.getItem(1).setTitle(getResources().getString(R.string.doing_tasks)+"(" + response.body().getCount() + ")");
                    }else{
                        MainActivity.menu.getItem(1).setTitle(getResources().getString(R.string.doing_tasks));
                    }
                    mProgressbar.setVisibility(View.GONE);
                    mSwipeRefreshLayout.setRefreshing(false);

                    if(response.body().getResults().isEmpty()){
                        mEmptyMsgView.visible();
                        mSwipeRefreshLayout.setRefreshing(false);
                    }
                    mLinerLayoutManager = new LinearLayoutManager(getActivity());
                    mLinerLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
                    try {

                        mRecyclerView = view.findViewById(R.id.recyclerview);
                        madapter = new RecyclerAdapterDoingTasks(getActivity());
                        madapter.setSchedule(response.body().getResults());
                        mRecyclerView.setAdapter(madapter);
                        mRecyclerView.setHasFixedSize(true);
                        mRecyclerView.setLayoutManager(mLinerLayoutManager);
                        mRecyclerView.setNestedScrollingEnabled(false);
                    } catch (Exception e) {

                    }
                }else {
                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        Toast.makeText(getActivity().getApplicationContext(), jObjError.getString("detail"), Toast.LENGTH_LONG).show();
                    } catch (Exception e) {
                        Toast.makeText(getActivity().getApplicationContext(), getResources().getString(R.string.please_try_again), Toast.LENGTH_LONG).show();
                        Log.i("Error", "Retrofit error" + e.getMessage());
                    }
                }

            }

            @Override
            public void onFailure(Call<Tasks> call, Throwable t) {
                mProgressbar.setVisibility(View.GONE);
                mSwipeRefreshLayout.setRefreshing(false);
                Log.i("error", "error" + t.getMessage());
                Toast.makeText(getActivity().getApplicationContext(), getResources().getString(R.string.please_try_again), Toast.LENGTH_SHORT).show();
            }
        });
    }
    public void addNewTask(View view) {
        Intent i = new Intent(getActivity().getApplicationContext(), AddNewTaskActivity.class);
        startActivity(i);
        getActivity().finish();

    }
}