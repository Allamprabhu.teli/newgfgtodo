package com.goflamingo.apps.gfgEmployees.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.goflamingo.apps.gfgEmployees.AddNewTaskActivity;
import com.goflamingo.apps.gfgEmployees.MainActivity;
import com.goflamingo.apps.gfgEmployees.R;
import com.goflamingo.apps.gfgEmployees.adapter.RecyclerAdapterDoingTasks;
import com.goflamingo.apps.gfgEmployees.adapter.RecyclerAdapterDoneTasks;
import com.goflamingo.apps.gfgEmployees.model.AllTasks;
import com.goflamingo.apps.gfgEmployees.model.Tasks;
import com.goflamingo.apps.gfgEmployees.network.GFGNetworkAPIService;
import com.goflamingo.apps.gfgEmployees.util.CacheManager;
import com.goflamingo.apps.gfgEmployees.util.EmptyMsgView;
import com.goflamingo.apps.gfgEmployees.util.PaginationCustom;

import org.json.JSONObject;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DoneFragment extends android.app.Fragment
{
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    public static String done_count="0";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    //    private TextView mEmptyText;
//    private ImageView mEmptyImage;
//
//    private View view, myLayout;
    private View view;
    private CacheManager mCachmanger;
    private RecyclerView mRecyclerView;
    private ProgressBar mProgressbar;
    private FragmentActivity listener;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private DoingFragment.OnFragmentInteractionListener mListener;
    private LinearLayoutManager mLinerLayoutManager;
    private RecyclerAdapterDoneTasks madapter;
    private EmptyMsgView mEmptyMsgView;
    private boolean isPageLoading = false;
    private ArrayList<AllTasks> mDoneTasksList;
    public int finalMTotalItemsCount = 0;
    public DoneFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment AllTasksFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static DoneFragment newInstance(String param1, String param2) {
        DoneFragment fragment = new DoneFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_done, container, false);
        mCachmanger = new CacheManager(getActivity());
        mRecyclerView = view.findViewById(R.id.recyclerview);
        mProgressbar = view.findViewById(R.id.progressBar);
        mEmptyMsgView = new EmptyMsgView(getActivity(),view.findViewById(R.id.done_tasks_layout),"Tasks are not completed yet");
        mSwipeRefreshLayout = view.findViewById(R.id.done_swipe);
        mSwipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                allDoneTasks(1);
            }
        });

        allDoneTasks(1);
//        listener.setTitle(getResources().getString(R.string.toolbar_payments));
        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof Activity) {
            this.listener = (FragmentActivity) context;
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
    private void allDoneTasks(final int page) {
        mProgressbar.setVisibility(View.VISIBLE);
        mEmptyMsgView.invisible();
        GFGNetworkAPIService.DeviceApi apiService = GFGNetworkAPIService
                .getInstance().newTransaction(getActivity().getApplicationContext());
        Call<Tasks> call = apiService.getDoneTasks("Done",mCachmanger.getEmployeepk());
        call.enqueue(new Callback<Tasks>() {
            @Override
            public void onResponse(Call<Tasks> call, Response<Tasks> response) {
                if(!call.isCanceled() && response.isSuccessful()){


                        if (!response.body().getCount().equals("0")) {
                            MainActivity.menu.getItem(2).setTitle(getResources().getString(R.string.done_tasks) + "(" + response.body().getCount() + ")");
                        } else {
                            MainActivity.menu.getItem(2).setTitle(getResources().getString(R.string.done_tasks));
                        }
                            mProgressbar.setVisibility(View.GONE);
                            mSwipeRefreshLayout.setRefreshing(false);
                    if(page==1) {

                        if (response.body().getResults().isEmpty()) {
                            mEmptyMsgView.visible();
                            mSwipeRefreshLayout.setRefreshing(false);
                        }
                        mDoneTasksList = response.body().getResults();
                        finalMTotalItemsCount = Integer.parseInt(response.body().getCount());

                        mLinerLayoutManager = new LinearLayoutManager(getActivity());
                        mLinerLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
                        try {

                            mRecyclerView = view.findViewById(R.id.recyclerview);
                            madapter = new RecyclerAdapterDoneTasks(getActivity());
                            madapter.setSchedule(mDoneTasksList);
                            mRecyclerView.setAdapter(madapter);
                            mRecyclerView.setHasFixedSize(true);
                            mRecyclerView.setLayoutManager(mLinerLayoutManager);
                            mRecyclerView.setNestedScrollingEnabled(false);
                        } catch (Exception e) {

                        }
                        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                            @Override
                            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                                super.onScrollStateChanged(recyclerView, newState);
                            }

                            @Override
                            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                                super.onScrolled(recyclerView, dx, dy);
                                if(dy>0){
                                    if(!isPageLoading && mLinerLayoutManager.findLastCompletelyVisibleItemPosition() > mDoneTasksList.size() - 5 && mDoneTasksList.size()<finalMTotalItemsCount){
                                        isPageLoading = true;
                                        int mPageIndex = PaginationCustom.returnPageIndex(mDoneTasksList.size(), mCachmanger.getPaginationSize());
                                        allDoneTasks(mPageIndex);
                                    }
                                }
                            }
                        });
                    }else {
                        mDoneTasksList.addAll(response.body().getResults());
                        madapter.setSchedule(mDoneTasksList);
                        madapter.notifyDataSetChanged();
                        isPageLoading = false;
                    }
                }else {
                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        Toast.makeText(getActivity().getApplicationContext(), jObjError.getString("detail"), Toast.LENGTH_LONG).show();
                    } catch (Exception e) {
                        Toast.makeText(getActivity().getApplicationContext(), getResources().getString(R.string.please_try_again), Toast.LENGTH_LONG).show();
                        Log.i("Error", "Retrofit error" + e.getMessage());
                    }
                }

            }

            @Override
            public void onFailure(Call<Tasks> call, Throwable t) {
                mProgressbar.setVisibility(View.GONE);
                mSwipeRefreshLayout.setRefreshing(false);
                Log.i("error", "error" + t.getMessage());
                Toast.makeText(getActivity().getApplicationContext(), getResources().getString(R.string.please_try_again), Toast.LENGTH_SHORT).show();
            }
        });
    }
    public void addNewTask(View view) {
        Intent i = new Intent(getActivity().getApplicationContext(), AddNewTaskActivity.class);
        startActivity(i);
        getActivity().finish();
    }
}

