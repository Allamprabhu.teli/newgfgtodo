package com.goflamingo.apps.gfgEmployees.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.FragmentActivity;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.goflamingo.apps.gfgEmployees.AddNewTaskActivity;
import com.goflamingo.apps.gfgEmployees.MainActivity;
import com.goflamingo.apps.gfgEmployees.R;
import com.goflamingo.apps.gfgEmployees.adapter.RecyclerAdapterAllTasksList;
import com.goflamingo.apps.gfgEmployees.adapter.RecyclerAdapterToDoTasks;
import com.goflamingo.apps.gfgEmployees.model.Tasks;
import com.goflamingo.apps.gfgEmployees.network.GFGNetworkAPIService;
import com.goflamingo.apps.gfgEmployees.util.CacheManager;
import com.goflamingo.apps.gfgEmployees.util.EmptyMsgView;

import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ToDoFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ToDoFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ToDoFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    public static String todo_count="0";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private View view;
    private RecyclerView mRecentSchRecyclerView;
    private CacheManager mCachmanger;
    private LinearLayoutManager linearLayoutManagerm;
    RecyclerAdapterToDoTasks madapter;
    private EmptyMsgView mEmptymsgView;
    private ProgressBar mProgressbar;
    private boolean isPageLoading=false;
    public int finalMTotalItemsCount=0;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private OnFragmentInteractionListener mListener;
    private FragmentActivity listener;

    public ToDoFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ToDoFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ToDoFragment newInstance(String param1, String param2) {
        ToDoFragment fragment = new ToDoFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_to_do, container, false);
        mCachmanger = new CacheManager(getActivity());
        mSwipeRefreshLayout = view.findViewById(R.id.todo_swipe);
        mEmptymsgView = new EmptyMsgView(getActivity(),view.findViewById(R.id.to_do_tasks_layout),"No tasks are available");
        mProgressbar = view.findViewById(R.id.progressBar);
        mProgressbar.setVisibility(View.GONE);
        mSwipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                toDOTaskList();
            }
        });



        toDOTaskList();
        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof Activity) {
            this.listener = (FragmentActivity) context;
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    private void toDOTaskList() {
        mProgressbar.setVisibility(View.VISIBLE);
        mEmptymsgView.invisible();
        GFGNetworkAPIService.DeviceApi apiService = GFGNetworkAPIService
                .getInstance().newTransaction(getActivity().getApplicationContext());
        Call<Tasks> call = apiService.getToDoTasks("ToDo",mCachmanger.getEmployeepk());
        call.enqueue(new Callback<Tasks>() {
            @Override
            public void onResponse(Call<Tasks> call, Response<Tasks> response) {
                if(!call.isCanceled() && response.isSuccessful()){

                    if(!response.body().getCount().equals("0")){
                        MainActivity.menu.getItem(0).setTitle(getResources().getString(R.string.todo_Tasks)+"("+response.body().getCount()+")");
                    }else{
                        MainActivity.menu.getItem(0).setTitle(getResources().getString(R.string.todo_Tasks));

                    }
                    mProgressbar.setVisibility(View.GONE);
                    mSwipeRefreshLayout.setRefreshing(false);

                    if(response.body().getResults().isEmpty()){
                        mEmptymsgView.visible();
                        mSwipeRefreshLayout.setRefreshing(false);
                    }
                    linearLayoutManagerm = new LinearLayoutManager(getActivity());
                    linearLayoutManagerm.setOrientation(LinearLayoutManager.VERTICAL);
                    try {

                        mRecentSchRecyclerView = view.findViewById(R.id.recyclerview);
                        madapter = new RecyclerAdapterToDoTasks(getActivity());
                        madapter.setSchedule(response.body().getResults());
                        mRecentSchRecyclerView.setAdapter(madapter);
                        mRecentSchRecyclerView.setHasFixedSize(true);
                        mRecentSchRecyclerView.setLayoutManager(linearLayoutManagerm);
                        mRecentSchRecyclerView.setNestedScrollingEnabled(false);
                    } catch (Exception e) {

                    }
                }else {
                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        Toast.makeText(getActivity().getApplicationContext(), jObjError.getString("detail"), Toast.LENGTH_LONG).show();
                    } catch (Exception e) {
                        Toast.makeText(getActivity().getApplicationContext(), getResources().getString(R.string.please_try_again), Toast.LENGTH_LONG).show();
                        Log.i("Error", "Retrofit error" + e.getMessage());
                    }
                }
            }
            @Override
            public void onFailure(Call<Tasks> call, Throwable t) {
                mProgressbar.setVisibility(View.GONE);
                mSwipeRefreshLayout.setRefreshing(false);
                Log.i("error", "error" + t.getMessage());
                Toast.makeText(getActivity().getApplicationContext(), getResources().getString(R.string.please_try_again), Toast.LENGTH_SHORT).show();
            }
        });
    }
    public void addNewTask(View view) {
        Intent i = new Intent(getActivity().getApplicationContext(), AddNewTaskActivity.class);
        startActivity(i);
        getActivity().finish();

    }
}
