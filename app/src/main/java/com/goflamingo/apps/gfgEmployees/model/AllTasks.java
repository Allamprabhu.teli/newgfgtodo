package com.goflamingo.apps.gfgEmployees.model;


public class AllTasks {
    private String assigned_date;

    private FromEmployee from_emp;

    private String id;

    private String task_type;

    private Points point;

    private ToEmployee to_emp;

    private String desc;

    private String status;

    public String getAssigned_date() {
        return assigned_date;
    }

    public void setAssigned_date(String assigned_date) {
        this.assigned_date = assigned_date;
    }

    public FromEmployee getFrom_emp() {
        return from_emp;
    }

    public void setFrom_emp(FromEmployee from_emp) {
        this.from_emp = from_emp;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTask_type() {
        return task_type;
    }

    public void setTask_type(String task_type) {
        this.task_type = task_type;
    }

    public Points getPoint() {
        return point;
    }

    public void setPoint(Points point) {
        this.point = point;
    }

    public ToEmployee getTo_emp() {
        return to_emp;
    }

    public void setTo_emp(ToEmployee to_emp) {
        this.to_emp = to_emp;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "AllTasks{" +
                "assigned_date='" + assigned_date + '\'' +
                ", from_emp=" + from_emp +
                ", id='" + id + '\'' +
                ", task_type='" + task_type + '\'' +
                ", point='" + point + '\'' +
                ", to_emp=" + to_emp +
                ", desc='" + desc + '\'' +
                ", status='" + status + '\'' +
                '}';
    }
}
