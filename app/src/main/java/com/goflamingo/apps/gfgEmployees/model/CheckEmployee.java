package com.goflamingo.apps.gfgEmployees.model;

import javax.crypto.spec.DESedeKeySpec;

public class CheckEmployee {
    private String date;

    private String otp_msg;

    private boolean is_preferred;

    private String emp_status;

    private String otp_sender_id;

    private String name;

    private String mobile;

    private Department dept;

    private String id;

    private String user;

    private String email;

    public String getDate ()
    {
        return date;
    }

    public void setDate (String date)
    {
        this.date = date;
    }

    public String getOtp_msg ()
    {
        return otp_msg;
    }

    public void setOtp_msg (String otp_msg)
    {
        this.otp_msg = otp_msg;
    }

    public boolean getIs_preferred ()
    {
        return is_preferred;
    }

    public void setIs_preferred (boolean is_preferred)
    {
        this.is_preferred = is_preferred;
    }

    public String getEmp_status ()
    {
        return emp_status;
    }

    public void setEmp_status (String emp_status)
    {
        this.emp_status = emp_status;
    }

    public String getOtp_sender_id ()
    {
        return otp_sender_id;
    }

    public void setOtp_sender_id (String otp_sender_id)
    {
        this.otp_sender_id = otp_sender_id;
    }

    public String getName ()
    {
        return name;
    }

    public void setName (String name)
    {
        this.name = name;
    }

    public String getMobile ()
    {
        return mobile;
    }

    public void setMobile (String mobile)
    {
        this.mobile = mobile;
    }

    public Department getDept ()
    {
        return dept;
    }

    public void setDept (Department dept)
    {
        this.dept = dept;
    }

    public String getId ()
    {
        return id;
    }

    public void setId (String id)
    {
        this.id = id;
    }

    public String getUser ()
    {
        return user;
    }

    public void setUser (String user)
    {
        this.user = user;
    }

    public String getEmail ()
    {
        return email;
    }

    public void setEmail (String email)
    {
        this.email = email;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [date = "+date+", otp_msg = "+otp_msg+", is_preferred = "+is_preferred+", emp_status = "+emp_status+", otp_sender_id = "+otp_sender_id+", name = "+name+", mobile = "+mobile+", dept = "+dept+", id = "+id+", user = "+user+", email = "+email+"]";
    }
}
