package com.goflamingo.apps.gfgEmployees.model;


import java.util.ArrayList;

public class EmployeeConfig
{
    private Update update;
    private Employees employee;
    private ArrayList<GFGEmployees> other_employees;

    private ArrayList<Points>  point;

    public ArrayList<Points> getPoint() {
        return point;
    }

    public void setPoint(ArrayList<Points> point) {
        this.point = point;
    }

    public ArrayList<GFGEmployees> getOther_employees() {
        return other_employees;
    }

    public void setOther_employees(ArrayList<GFGEmployees> other_employees) {
        this.other_employees = other_employees;
    }

    public Update getUpdate() {
        return update;
    }

    public void setUpdate(Update update) {
        this.update = update;
    }

    public Employees getEmployee() {
        return employee;
    }

    public void setEmployee(Employees emp) {
        this.employee = emp;
    }


    @Override
    public String toString() {
        return "EmployeeConfig{" +
                "update=" + update +
                ", emp=" + employee +
                ", other_employees=" + other_employees +
                '}';
    }
}
