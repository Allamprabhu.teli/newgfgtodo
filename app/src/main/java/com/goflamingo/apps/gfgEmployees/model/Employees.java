package com.goflamingo.apps.gfgEmployees.model;

public class Employees
{
        private Department dept;
        private String name;
        private boolean is_preferred;
        private String emp_status;
        private String id;

    public Department getDept() {
        return dept;
    }

    public void setDept(Department dept) {
        this.dept = dept;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isIs_preferred() {
        return is_preferred;
    }

    public void setIs_preferred(boolean is_preferred) {
        this.is_preferred = is_preferred;
    }

    public String getEmp_status() {
        return emp_status;
    }

    public void setEmp_status(String emp_status) {
        this.emp_status = emp_status;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Employees{" +
                "dept=" + dept +
                ", name='" + name + '\'' +
                ", is_preferred=" + is_preferred +
                ", emp_status='" + emp_status + '\'' +
                ", id='" + id + '\'' +
                '}';
    }
}
