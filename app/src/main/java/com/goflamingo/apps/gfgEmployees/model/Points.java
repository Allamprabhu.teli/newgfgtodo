package com.goflamingo.apps.gfgEmployees.model;

public class Points
{
    private String id;
    private String point;
    private String name;

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPoint() {
        return point;
    }

    public void setPoint(String point) {
        this.point = point;
    }

    @Override
    public String toString() {
        return "Points{" +
                "id='" + id + '\'' +
                ", point='" + point + '\'' +
                ", name='" + name + '\'' +
                '}';
    }
}
