package com.goflamingo.apps.gfgEmployees.model;

import java.util.ArrayList;

public class Tasks
{
    private String next;

    private String previous;

    private String count;

    private ArrayList<AllTasks> results;

    public String getNext() {
        return next;
    }

    public void setNext(String next) {
        this.next = next;
    }

    public String getPrevious() {
        return previous;
    }

    public void setPrevious(String previous) {
        this.previous = previous;
    }

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }

    public ArrayList<AllTasks> getResults() {
        return results;
    }

    public void setResults(ArrayList<AllTasks> results) {
        this.results = results;
    }

    @Override
    public String toString() {
        return "Tasks{" +
                "next='" + next + '\'' +
                ", previous='" + previous + '\'' +
                ", count='" + count + '\'' +
                ", results=" + results +
                '}';
    }
}