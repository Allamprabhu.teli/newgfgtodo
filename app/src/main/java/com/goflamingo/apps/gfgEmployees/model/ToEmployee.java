package com.goflamingo.apps.gfgEmployees.model;
public class ToEmployee
{
    private String date;

    private String is_preferred;

    private String emp_status;

    private String name;

    private String mobile;

    private String id;

    private String dept;

    private String user;

    private String email;

    public String getDate ()
    {
        return date;
    }

    public void setDate (String date)
    {
        this.date = date;
    }

    public String getIs_preferred ()
    {
        return is_preferred;
    }

    public void setIs_preferred (String is_preferred)
    {
        this.is_preferred = is_preferred;
    }

    public String getEmp_status ()
    {
        return emp_status;
    }

    public void setEmp_status (String emp_status)
    {
        this.emp_status = emp_status;
    }

    public String getName ()
    {
        return name;
    }

    public void setName (String name)
    {
        this.name = name;
    }

    public String getMobile ()
    {
        return mobile;
    }

    public void setMobile (String mobile)
    {
        this.mobile = mobile;
    }

    public String getId ()
    {
        return id;
    }

    public void setId (String id)
    {
        this.id = id;
    }

    public String getDept ()
    {
        return dept;
    }

    public void setDept (String dept)
    {
        this.dept = dept;
    }

    public String getUser ()
    {
        return user;
    }

    public void setUser (String user)
    {
        this.user = user;
    }

    public String getEmail ()
    {
        return email;
    }

    public void setEmail (String email)
    {
        this.email = email;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [date = "+date+", is_preferred = "+is_preferred+", emp_status = "+emp_status+", name = "+name+", mobile = "+mobile+", id = "+id+", dept = "+dept+", user = "+user+", email = "+email+"]";
    }
}



