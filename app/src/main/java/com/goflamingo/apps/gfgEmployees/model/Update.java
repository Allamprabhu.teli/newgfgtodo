package com.goflamingo.apps.gfgEmployees.model;

public class Update {
    private String link;

    private String app_version;
    private int pagination_count;

    public int getPagination_count() {
        return pagination_count;
    }

    public void setPagination_count(int pagination_count) {
        this.pagination_count = pagination_count;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getApp_version() {
        return app_version;
    }

    public void setApp_version(String app_version) {
        this.app_version = app_version;
    }

    @Override
    public String toString() {
        return "ClassPojo [link = " + link + ", app_version = " + app_version + "]";
    }
}
