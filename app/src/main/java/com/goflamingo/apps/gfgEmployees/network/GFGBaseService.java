package com.goflamingo.apps.gfgEmployees.network;

import android.content.Context;

import com.goflamingo.apps.gfgEmployees.BuildConfig;
import com.readystatesoftware.chuck.ChuckInterceptor;

import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;

public abstract class GFGBaseService<T>  {
    public T newTransaction(Context context) {
        OkHttpClient.Builder okHttpClientBuilder = new OkHttpClientBuilderFactory().getNewOkHttpClient()
                .connectTimeout(20, TimeUnit.SECONDS)
                .readTimeout(30, TimeUnit.SECONDS)
                .addInterceptor(new GFGTransactionInterceptor());

        Interceptor logger = HttpLogger.getLoggingInterceptor();
        if (logger != null) {
            okHttpClientBuilder.addInterceptor(logger);
        }
//        chuck shows ui with request/response
        if (BuildConfig.DEBUG) {
            okHttpClientBuilder.addInterceptor(new ChuckInterceptor(context)
                    .showNotification(true));
        }
        return getService(okHttpClientBuilder.build());
    }

    public abstract String getBaseApiUrl();

    abstract protected T getService(OkHttpClient okHttpClient);
}
