package com.goflamingo.apps.gfgEmployees.network;

import com.goflamingo.apps.gfgEmployees.BuildConfig;
import com.goflamingo.apps.gfgEmployees.model.AllTasks;
import com.goflamingo.apps.gfgEmployees.model.CheckEmployee;
import com.goflamingo.apps.gfgEmployees.model.EmployeeConfig;
import com.goflamingo.apps.gfgEmployees.model.Employees;
import com.goflamingo.apps.gfgEmployees.model.Tasks;
import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;


import java.util.ArrayList;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Query;

public class GFGNetworkAPIService extends GFGBaseService<GFGNetworkAPIService.DeviceApi> {

    public static String BASE_API_URL = "http://139.162.12.150/api/"; // production
    private static GFGNetworkAPIService deviceService;

    static {
        if (BuildConfig.DEBUG) {
            //BASE_API_URL = "http://192.168.0.116:8000/api/"; //local
            BASE_API_URL = "http://192.168.1.9:8000/api/";
        } else {
            BASE_API_URL = "http://192.168.1.9:8000/api/";// production
        }
    }

    public static synchronized GFGNetworkAPIService getInstance() {
        if (deviceService == null) {
            deviceService = new GFGNetworkAPIService();
        }
        return deviceService;
    }

    @Override
    public String getBaseApiUrl() {
        return BASE_API_URL;
    }

    @Override
    protected DeviceApi getService(OkHttpClient okHttpClient) {
        Gson gson = new GsonBuilder()
                .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
                .create();
        return new Retrofit.Builder()
                .baseUrl(getBaseApiUrl())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(okHttpClient)
                .build().create(DeviceApi.class);
    }

    public interface DeviceApi {

        @GET("empConfig")
        @Headers("Content-Type: application/json")
        Call<EmployeeConfig> employeeConfg(@Query("employee") String emp);

        @GET("checkEmp")
        @Headers("Content-Type: application/json")
        Call<CheckEmployee>  checkEmployee(@Query("username") String employee);

        @GET("task")
        @Headers("Content-Type: application/json")
        Call<Tasks>  getAllTasks(@Query("page") int page,@Query("to_emp") String employee_pk);

        @GET("task")
        @Headers("Content-Type: application/json")
        Call<Tasks>  getToDoTasks(@Query("status") String status,@Query("to_emp") String toEmployee);

        @GET("task")
        @Headers("Content-Type: application/json")
        Call<Tasks>  getDoingTasks(@Query("status") String status,@Query("to_emp") String toEmployee);

        @GET("task")
        @Headers("Content-Type: application/json")
        Call<Tasks>  getDoneTasks(@Query("status") String status,@Query("to_emp") String toEmployee);

        @GET("task")
        @Headers("Content-Type: application/json")
        Call<Tasks>  getAlertDoneTasks(@Query("status") String status,@Query("from_emp") String toEmployee);

        @POST("taskAssign/")
        @FormUrlEncoded
        Call<Tasks> postTask(@Field("from_pk") String fromUser, @Field("to_pk") String toUser, @Field("desc") String desc, @Field("point") String point);

        @POST("task/")
        @FormUrlEncoded
        Call<Tasks> postStatus(@Field("status") String status , @Field("from_pk") String fromEmployee);

        @POST("fcmPost/")
        @FormUrlEncoded
        Call<String> postFCMToken(@Field("fcm_token") String token, @Field("user_id") String user_id,@Field("phone_mac") String mac_id);

        @POST("taskUpdate/")
        @FormUrlEncoded
        Call<AllTasks> postEndStatus(@Field("task_pk") String task , @Field("status") String status, @Field("emp_pk") String Emoloyee, @Field("high_five") String high_five_employee);

        @POST("task_reassign/")
        @FormUrlEncoded
        Call<Tasks> reassignToSomeOne(@Field("taskhead_pk") String task , @Field("from_pk") String fromEmployee,@Field("to_pk") String to_emloyee,@Field("status") String status);
    }
}
