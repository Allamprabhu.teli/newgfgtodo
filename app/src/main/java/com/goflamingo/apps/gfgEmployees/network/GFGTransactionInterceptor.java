package com.goflamingo.apps.gfgEmployees.network;

import android.util.Base64;

import java.io.IOException;

import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

public class GFGTransactionInterceptor implements Interceptor {

    private final String USER_NAME = "gfg";
    private final String PASSWORD = "gfgtech123";

    @Override
    public Response intercept(Chain chain) throws IOException {
        Request originalRequest = chain.request();

        HttpUrl url = originalRequest
                .url()
                .newBuilder()
                .build();

        Request.Builder newRequestBuilder = originalRequest.newBuilder();

        String credentials = String.format("%s:%s", USER_NAME, PASSWORD);
        final String basic = "Basic " +
                Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);

        newRequestBuilder.removeHeader("Authorization")
                .addHeader("Authorization", basic);
        newRequestBuilder.removeHeader("Accept")
                .addHeader("Accept", "application/json");
        newRequestBuilder.url(url);

        return chain.proceed(newRequestBuilder.build());
    }
}