package com.goflamingo.apps.gfgEmployees.network;


import com.goflamingo.apps.gfgEmployees.BuildConfig;
import okhttp3.Interceptor;
import okhttp3.logging.HttpLoggingInterceptor;


public class HttpLogger {
    public static Interceptor getLoggingInterceptor() {
        HttpLoggingInterceptor logger = new HttpLoggingInterceptor();
        logger.setLevel(BuildConfig.DEBUG ? HttpLoggingInterceptor.Level.BODY
                : HttpLoggingInterceptor.Level.BASIC);
        return logger;
    }
}