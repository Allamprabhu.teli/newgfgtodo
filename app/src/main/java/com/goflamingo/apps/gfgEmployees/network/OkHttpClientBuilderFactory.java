package com.goflamingo.apps.gfgEmployees.network;

import com.facebook.stetho.okhttp3.StethoInterceptor;
import com.goflamingo.apps.gfgEmployees.BuildConfig;

import okhttp3.OkHttpClient;

class OkHttpClientBuilderFactory {
    public OkHttpClient.Builder getNewOkHttpClient() {
        OkHttpClient.Builder okHttpClientBuilder = new OkHttpClient.Builder();
        if (BuildConfig.DEBUG) {
            okHttpClientBuilder.addNetworkInterceptor(new StethoInterceptor());
        }
        return okHttpClientBuilder;
    }
}
