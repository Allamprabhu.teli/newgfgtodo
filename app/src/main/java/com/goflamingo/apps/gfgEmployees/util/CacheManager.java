package com.goflamingo.apps.gfgEmployees.util;


import android.content.Context;
import android.content.SharedPreferences;

import com.goflamingo.apps.gfgEmployees.model.Employees;
import com.goflamingo.apps.gfgEmployees.model.GFGEmployees;
import com.goflamingo.apps.gfgEmployees.model.Points;
import com.google.android.gms.common.util.ArrayUtils;
import com.google.gson.Gson;

import java.util.ArrayList;

import retrofit2.http.POST;

import static android.content.ContentValues.TAG;

public class CacheManager {

    public static final int REQUEST_ID_MULTIPLE_PERMISSIONS = 1;
    private final SharedPreferences sharedPreferences;
    private Context context;
    private final Gson gson;
    public static ArrayList<GFGEmployees> EMPLOYEES = new ArrayList<GFGEmployees>();
    public static ArrayList<Points> POINTS = new ArrayList<Points>();

    public CacheManager(Context context) {
        sharedPreferences = context.getSharedPreferences(TAG, Context.MODE_PRIVATE);
        gson = JsonParser.getInstance().getParser();
    }
    public void setIsFirstTimeLogin(Boolean bool){
        sharedPreferences.edit()
                .putBoolean("is_first_time_login",bool)
                .apply();
    }
    public boolean isFirstTimeLogin(){
        return sharedPreferences.getBoolean("is_first_time_login",true);
    }
    public void setEmployeeDetails(String str){
        sharedPreferences.edit()
                .putString("employee_details",str)
                .apply();
    }
    public String getEmployeeDetails(){
        return sharedPreferences.getString("employee_details","");
    }
    public void setEmployeePk(String str){
        sharedPreferences.edit()
                .putString("employee_pk",str)
                .apply();
    }
    public String getEmployeepk(){
        return sharedPreferences.getString("employee_pk","");
    }
    public void setEmployees(ArrayList<GFGEmployees> emp_name){
        EMPLOYEES = emp_name;
    }
    public ArrayList<GFGEmployees> getEmployees(){
        return EMPLOYEES;
    }
    public void setEmployeeName(String str){
        sharedPreferences.edit()
                .putString("emp_name",str)
                .apply();
    }
    public String getEmployeeName(){
        return sharedPreferences.getString("emp_name","");
    }
    public void setPoints(ArrayList<Points> point){
        POINTS = point;
    }
    public ArrayList<Points> getPoints(){
        return POINTS;
    }
    public int getPaginationSize() {
        return sharedPreferences.getInt("page_size", 1);
    }

    public void setPaginationSize(int size) {
        sharedPreferences.edit()
                .putInt("page_size", size)
                .apply();
    }
    public void setIsTokenGenerated(boolean bool) {
        sharedPreferences.edit()
                .putBoolean("is_token_generated", bool)
                .apply();
    }
    public boolean isTokenGenerated() {
        return sharedPreferences.getBoolean("is_token_generated",false);
    }

    public void setIsTokenAddedToDB(boolean bool) {
        sharedPreferences.edit()
                .putBoolean("token_to_db", bool)
                .apply();
    }
    public boolean isTokenAddedToDB() {
        return sharedPreferences.getBoolean("token_to_db",false);
    }


    public void setFCMToken(String bool) {
        sharedPreferences.edit()
                .putString("fcm_token", bool)
                .apply();
    }
    public String getFCMToken() {
        return sharedPreferences.getString("fcm_token","");
    }
}
