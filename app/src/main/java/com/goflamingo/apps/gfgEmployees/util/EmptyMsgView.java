package com.goflamingo.apps.gfgEmployees.util;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.goflamingo.apps.gfgEmployees.R;


public class EmptyMsgView
{
   private TextView mEmptyText;
   private LinearLayout mLineareLayout;
   private ViewGroup mainLayout;
   private View myLayout;


   public EmptyMsgView(Activity activity, View resourceId, String msg)
   {
      mainLayout = (ViewGroup)resourceId.getParent();

      LayoutInflater inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
      myLayout = inflater.inflate(R.layout.layout_null_results, mainLayout, false);
      mainLayout.addView(myLayout);

      mEmptyText = myLayout.findViewById(R.id.empty_results_text);
      mLineareLayout = myLayout.findViewById(R.id.my_layout);
      mEmptyText.setText(msg);
   }

   public void invisible()
   {
      mLineareLayout.setVisibility(View.GONE);
   }
   public void visible(){
      mLineareLayout.setVisibility(View.VISIBLE);
      }


}
