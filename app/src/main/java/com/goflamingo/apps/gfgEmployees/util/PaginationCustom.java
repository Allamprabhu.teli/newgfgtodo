package com.goflamingo.apps.gfgEmployees.util;

public class PaginationCustom {
    public static int returnPageIndex(int sizeOfList, int pagination_size) {
        int index = (sizeOfList / pagination_size) + 1;
        return index;
    }
}
